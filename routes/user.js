const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth"); 

// Route checking if the user's email already exists in the database
router.get("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for setting user as admin - ADMIN
router.put("/:userId/setasadmin", auth.verify, (req, res) => {
    const data = {
        userId: req.params.userId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userController.setAsAdmin(data).then(resultFromController => res.send(resultFromController));
})

// Route for creating an order - NONADMIN
router.post("/checkout", auth.verify, (req, res) => {
    const data = {
        userId: auth.decode(req.headers.authorization).id,
        userEmail: auth.decode(req.headers.authorization).email,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        productName: req.body.productName,
        productQuantity: req.body.productQuantity,
        address: req.body.address,
        city: req.body.city,
        zipCode: req.body.zipCode,
        state: req.body.state,
        country: req.body.country
    }
    userController.checkout(data).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all orders - ADMIN
router.get("/orders", auth.verify, (req, res) => {
    const isAdmin = auth.decode(req.headers.authorization).isAdmin;
    userController.getOrders(isAdmin).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific user's orders
router.get("/myorders", auth.verify, (req, res) => {
    const userId = auth.decode(req.headers.authorization).id;
    userController.getMyOrders(userId).then(resultFromController => res.send(resultFromController));
})

module.exports = router;