const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth"); 
// const { text } = require("express");

// Route for creating a product - ADMIN
router.post("/", auth.verify, (req, res) => {
    const data = {
        name: req.body.name,
        brand: req.body.brand,
        category: req.body.category,
        description: req.body.description,
        price: req.body.price, 
        image: req.body.image,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        user: auth.decode(req.headers.authorization).id,
    }
    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all products
router.get("/all", (req, res) => {
    productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving a specific product
router.get("/:productId", (req, res) => {
    console.log(req.params.productId);
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product - ADMIN
router.put("/:productId", auth.verify, (req,res) => {
    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        name: req.body.name,
        brand: req.body.brand,
        category: req.body.category,
        description: req.body.description,
        price: req.body.price,
        isActive: req.body.isActive
    }
    productController.updateProduct(data).then(resultFromController => res.send(resultFromController));
}) 

// Route for archiving a product - ADMIN
router.put("/:productId/archive", auth.verify, (req, res) => {
    const data = {
        productId: req.params.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    productController.archiveProduct(data).then(resultFromController => res.send(resultFromController));
})

// Route for regular product search - REGISTERED USER
router.get("/search/:searchString/r", auth.verify, (req, res) => {
    const data = {
        // productName: new RegExp(regexRemoveWhiteSpace(req.params.searchString), 'i'),
        productName: req.params.searchString,
        userId: auth.decode(req.headers.authorization).id
    };
    productController.searchProduct(data).then(resultFromController => res.send(resultFromController));
})

// Route for fuzzy product search - REGISTERED USER
router.get("/search/:searchString/f", auth.verify, (req, res) => {
    const data = {
        // productName: new RegExp(regexRemoveWhiteSpace(req.params.searchString), 'i'),
        productName: req.params.searchString,
        userId: auth.decode(req.headers.authorization).id
    };
    productController.searchProductFuzzy(data).then(resultFromController => res.send(resultFromController));
})


module.exports = router;