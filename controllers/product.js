const Product = require("../models/Product");
const auth = require("../auth");

// Add product - ADMIN
module.exports.addProduct = (data) => {
    console.log(`isAdmin ${data.isAdmin}`);
    console.log(`data ${data}`);
    if(data.isAdmin) {
        let newProduct = new Product({
            name: data.name,
            brand: data.brand,
            category: data.category,
            description: data.description,
            price: data.price,
            image: data.image,
            user: data.user        
        });
        return newProduct.save().then((product, error) => {
            if (error) {
                return false;
            }
            else {
                return true;
            }
        })    
    }
    else {
        return Promise.resolve(false);
    }
}

// Retrieve all products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
}

// Retrieve specific product
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}

// Update a product - ADMIN
module.exports.updateProduct = (data) => {
    console.log(`isAdmin ${data.isAdmin}`);
    console.log(`productId ${data.productId}`);
    console.log(`data ${data}`);
    if(data.isAdmin) {
        let updatedProduct = {
            name: data.name,
            brand: data.brand,
            category: data.category,
            description: data.description,
            price: data.price    
        };
        return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
            if (error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// Archive a product - ADMIN
module.exports.archiveProduct = (data) => {
    console.log(`isAdmin ${data.isAdmin}`);
    console.log(`productId ${data.productId}`);
    if(data.isAdmin) {
        let updatedProduct = {
            isActive: false
        }
        return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
            if (error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// Regular search for products - REGISTERED USER
module.exports.searchProduct = (data) => {
    const productName = new RegExp(regexRemoveWhiteSpace(data.productName), 'i');
 
    console.log(`searchString ${productName}`);
    console.log(`userId ${data.userId}`);

    if(data.userId) {
        return Product.find({'name': productName}, 'name').then((foundProduct, error) => {
            if (error) {
                return false;
            }
            else {
                return foundProduct;
            }
        });
    }
    else {
        return Promise.resolve(false);
    }
}

// Fuzzy search for products - REGISTERED USER
module.exports.searchProductFuzzy = (data) => {
    const productName = new RegExp(regexFuzzy(data.productName), 'gi');
 
    console.log(`searchString ${productName}`);
    console.log(`userId ${data.userId}`);

    if(data.userId) {
        return Product.find({'description': productName}, 'name').then((foundProduct, error) => {
            if (error) {
                return false;
            }
            else {
                return foundProduct;
            }
        });
    }
    else {
        return Promise.resolve(false);
    }
}

// Function for replacing all whitespace with nothing multiple times
function regexRemoveWhiteSpace(sometext) {
    return sometext.replace(/\s/g, '');
}

// Helper function for fuzzy search
function regexFuzzy(sometext) {
    return sometext.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}