// imports the User model
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//imports the Product & Order model
const Product = require("../models/Product");
const Order = require("../models/Order");

// Check email duplicates (/checkEmail)
module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(result => {
        if(result.length > 0) {
            return true;
        }
        else {
            return false;
        }
    })
}

// User registration (/register)
module.exports.registerUser = (reqBody) => {
    return User.find({email : reqBody.email}).then(result => {
        if(result.length > 0) {
            return Promise.resolve(`${reqBody.email} is an existing user & cannot be registered again.`);
        }
        else {
            let newUser = new User ({
                name: reqBody.name,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 10)
            });
            return newUser.save().then((user, error) => {
                // User registration failed
                if(error){
                    return false;
                }
                // User registration successful
                else {
                    return true;
                }
            })
        }
    })
}    


// User authentication (/login)
module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(result => {
        // User does not exist
        if (result == null) {
            return false;
        }
        // User exists
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                // Generate an access token
                return { access : auth.createAccessToken(result)};
            }
            // Passwords do not match
            else {
                return false;
            }
        }
    })
}

// Set user as admin (/:userId/setasadmin)
module.exports.setAsAdmin = (data) => {
    console.log(`isAdmin ${data.isAdmin}`);
    console.log(`userId ${data.userId}`);
    if(data.isAdmin) {
        let updatedUser = {
            isAdmin: true
        };
        return User.findByIdAndUpdate(data.userId, updatedUser).then((user, error) => {
            if (error) {
                return false;
             }
            else {
                return true;
        }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// Create Order (/checkout) - NONADMIN
module.exports.checkout = async (data) => {
    // console.log(`isAdmin ${data.isAdmin}`);
    // console.log(`userId ${data.userId}`);
  
    if (!(data.isAdmin)) {
        return Product.findOne({'name': data.productName}).then((foundProduct, error) => {
            // console.log(foundProduct);
            if(error) {
                return Promise.resolve(`Cannot find on product database.`);
            }
            else {
                if (foundProduct == null) {
                    return Promise.resolve(`${data.productName} not found`);
                }
                else {
                    var productN = foundProduct['_id'];
                    var priceN = foundProduct['price'];
                    var totalAmountN = priceN * data.productQuantity;
                    let order = new Order ({
                            user: data.userId,
                            totalAmount: totalAmountN,
                            shippingAddress: [
                                {
                                    address: data.address,
                                    city: data.city,
                                    zipCode: data.zipCode,
                                    state: data.state,
                                    country: data.country                    
                                }
                            ], 
                            orderItems: [
                                {
                                    product: productN,
                                    quantity: data.productQuantity,
                                    price: priceN                    
                                }
                            ],
                        });
                    console.log(order);
                    return order.save().then((order, error) => {
                        if(error) {
                            // return Promise.resolve(false);
                            return Promise.resolve('Order not saved.');
                        }
                        else {
                            // return true;
                            return Promise.resolve(`Order entered for ${data.userEmail}! Please process payment for ${totalAmountN}. Thank you.`);
                        }
                    })
                }
            }
        }) 
    }
    else {
        // return false;
        return Promise.resolve('Administrators cannot place orders.');
    }
}   

// Retrieve all orders (/orders) - ADMIN
module.exports.getOrders = (isAdmin) => {
    if (isAdmin) {
        return Order.find({}).then((orders, error) => {
            if (error) {
                return false;
            }
            else {
                return orders;
            }
        }) 
    }
    else {
        return Promise.resolve(false);
    }
}

// Retrieve a specific user's orders (/myOrders)
module.exports.getMyOrders = (userId) => {
    if (userId) {
        return Order.find({'user': userId}).then((orders, error) => {
            if (error) {
                return false;
            }
            else {
                return orders;
            }
        }) 
    }
    else {
        return Promise.resolve(false);
    }
}