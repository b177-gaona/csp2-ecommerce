const express = require("express");
const mongoose = require('mongoose');
const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Set up mongoose connection
const mongoDB = "mongodb+srv://ky13_0pr0j3ct:QDukyuzuFSxJtjjX@cluster0.4kk1v.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority";
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true 
});

// Prompts a message for successful database connection 
mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas`));

// Allows all resources to access the backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes);

// Defines the "/product" string to be included for all product routes defined in the "product" route file
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

