const mongoose = require("mongoose");

const reviewSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    rating: {
        type: Number,
        required: true
    },
    comment: {
        type: String,
        required: true
    }
}, {
    timestamps: true
})

const productSchema = new mongoose.Schema({
    // Which admin created the product
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    name: {
        type: String,
        required: [true, "Product Name is required."]
    },
    image: {
        type: String,
        required: true
    },
    brand: {
        type: String,
        required: [true, "Product Brand is required."]
    },
    category: {
        type: String,
        required: [true, "Product Category is required."]
    },
    description: {
        type: String,
        required: [true, "Product Description is required."]
    },
    reviews: [reviewSchema],
    rating: {
        type: Number,
        required: true,
        default: 0
    },
    numReviews: {
        type: Number,
        required: true,
        default: 0
    },
    price: {
        type: Number,
        required: [true, "Product Price is required."]
    },
    countInStock: {
        type: Number,
        required: true,
        default: 0
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Product", productSchema);